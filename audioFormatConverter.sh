#!/bin/bash

directory=$1
originalFileType=$2
newFileType=$3
extraOptions=$4

if [ -n "$originalFileType" ]; then
	# Find all the songs with desired filetype in the chosen directory
	stringList=$(find $directory -type f -name "*.$originalFileType" -print)
else
	stringList=$(find $directory -type f -name "*.*" -print)
fi


# Make an array for the list of files
while read -r line; do 
	originalList+=("$line"); 
	newList+=("${line%.$originalFileType}$newFileType")
done <<<"$stringList"

#echo "${originalList[@]}"
#parallel ffmpeg -i {1} {2} ::: "${originalList[@]}" ::: "${newList[@]}"

parallel "ffmpeg -i {} $extraOptions  {}.$newFileType && rm {}" ::: "${originalList[@]}"

#for song in "${newList[@]}"; do
	#echo " $song"
	##echo " ${song%.$originalFileType}$newFileType"
	##ffmpeg -i "$song" "${song%.$originalFileType}$newFileType"
#done


#parallel ffmpeg $fileList 
